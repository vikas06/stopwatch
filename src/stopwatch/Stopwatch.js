
import React, { Component } from 'react';
import './stopwatch.scss';

class Stopwatch extends Component {
 constructor(props) {
   super(props);
   this.state = {
     timerOn: false,
     timerStart: 0,
     timerTime: 0
   };
 }

 startTimer = () => {
   this.setState({
     timerOn: true,
     timerTime: this.state.timerTime,
     timerStart: Date.now() - this.state.timerTime
   });
   this.timer = setInterval(() => {
     this.setState({
       timerTime: Date.now() - this.state.timerStart
     });
   }, 10);
 };

 stopTimer = () => {
   this.setState({ timerOn: false });
   clearInterval(this.timer);
 };
 resetTimer = () => {
   this.setState({
     timerStart: 0,
     timerTime: 0
   });
 };

 render() {
   const { timerTime } = this.state;
   let centiseconds = ('0' + (Math.floor(timerTime / 10) % 100)).slice(-2);
   let seconds = ('0' + (Math.floor(timerTime / 1000) % 60)).slice(-2);
   let minutes = ('0' + (Math.floor(timerTime / 60000) % 60)).slice(-2);
   let hours = ('0' + Math.floor(timerTime / 3600000)).slice(-2);
   return (
     <div className='Sw-main'>
     <div className='Sw-main__header'>
       <header style={{color:"yellow", fontStyle:"italic"}}><h2>Kaam24 </h2></header>
     </div>
       <div className='Sw-main__Stopwatch-display'>
         <p className='Sw-main__Stopwatch-display__para'>Stopwatch</p>
         {hours} : {minutes} : {seconds} : {centiseconds}
       </div>
       <div className='Sw-main__buttonContainer'>
       {this.state.timerOn === false && this.state.timerTime === 0 && (
         <button  className='Sw-main__buttonContainer__button' onClick={this.startTimer}>Start</button>
       )}
       {this.state.timerOn === true && (
         <button className='Sw-main__buttonContainer__button' onClick={this.stopTimer}>Stop</button>
       )}
       {this.state.timerOn === false && this.state.timerTime > 0 && (
         <button className='Sw-main__buttonContainer__button' onClick={this.startTimer}>Resume</button>
       )}
       {this.state.timerOn === false && this.state.timerTime > 0 && (
         <button className='Sw-main__buttonContainer__button' onClick={this.resetTimer}>Reset</button>
       )}
       </div>
     </div>
   );
 }
}

export default Stopwatch;